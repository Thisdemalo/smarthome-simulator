package com.smarthomesimulator.smarthomesimulator.Controller;

import com.smarthomesimulator.smarthomesimulator.DataGenerator.DayData;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@EnableScheduling
@RestController
public class Simulator {

    private DayData d;

    public Simulator(){
        this.d = new DayData();
    }

    //@Scheduled(fixedDelay = 1000)
    @GetMapping("/")
    public float getTemperature(){
        d.generateTemp();
        return d.getTemp();
    }


}
