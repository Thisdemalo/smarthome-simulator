package com.smarthomesimulator.smarthomesimulator.DataGenerator;

import java.util.Random;

public class DayData {

    private float nowTemp = 0.2f;
    private int minTemp = -10;
    private int maxTemp = 45;
    private float variationTemp = 1.89f;

    public DayData(){
        nowTemp = (float) Math.random() * ( maxTemp - minTemp );
    }

    public void generateTemp(){
        Random rand = new Random();
        float var = 0.0f;

        if(rand.nextBoolean())
            var = (float) Math.random() * ( 0 - variationTemp );
        else
            var = (float) Math.random() * ( variationTemp );

        nowTemp = nowTemp + var;
    }

    public float getTemp(){
        return this.nowTemp;
    }






}
