package com.smarthomesimulator.smarthomesimulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class SmarthomeSimulatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmarthomeSimulatorApplication.class, args);
    }

    @GetMapping("/test")
    public int test(){

        System.out.println("TEST");


        return 587;

    }
}
